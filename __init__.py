from collections import OrderedDict as odict

implementations = odict()

def implements(hook):

    global implementations
    
    if not implementations.has_key(hook):
        implementations[hook] = odict()

    def decorator(func):

        global implementations

        id = "%s:%s" % (func.__module__, func.__name__)
        implementations[hook][id] = {
            'id': id,
            'callback': func
        }

        return func

    return decorator


def invoke(hook, *args, **kwargs):

    ret = {}

    if implementations.has_key(hook):
        for id, impl in implementations[hook].iteritems():
            ret[impl['id']] = impl['callback'](*args, **kwargs)

    return ret

